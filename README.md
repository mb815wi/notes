# React.js Notes

Dillinger is a cloud-enabled, mobile-ready, offline-storage, AngularJS powered HTML5 Markdown editor.

  - Extract/clone files to dir
  - Run "npm install" in the extracted folder
  - Run "npm install -g json-server" in the extracted folder
  - Run "json-server --watch db.json" to start API 
  - Run "npm start" to view the project


### Installation

Install the dependencies
```sh
$ cd project_dir
$ npm install
$ npm install -g json-server
$ json-server --watch db.json
$ npm start
```

