import React, { Component } from 'react';
import { Route, NavLink } from 'react-router-dom';

import './Home.css';

import Notes from './Notes/Notes';
import NewNote from './NewNote/NewNote';
import FullNote from './FullNote/FullNote';
import EditNote from './EditNote/EditNote';

import { renderToStaticMarkup } from "react-dom/server";
import { withLocalize } from "react-localize-redux";
import globalTranslations from "./Translations/global.json";
import LanguageToggle from './Translations/LanguageToggle';
import { Translate } from "react-localize-redux";

class Home extends Component {

    constructor(props) {
        super(props);

        this.props.initialize({
            languages: [
                { name: "Slovak", code: "sk" },
                { name: "English", code: "en" }
            ],
            translation: globalTranslations,
            options: { renderToStaticMarkup }
        });
    }

    render() {
 
        return (
            <div className="Home">
                <header>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item mr-1"><NavLink
                                to="/"
                                className='btn btn-outline-secondary'
                                exact>
                                <Translate id="notes"></Translate>
                            </NavLink></li>
                            <li className="nav-item mr-1">
                                <NavLink 
                                    to={{pathname: '/new-post'}}
                                    className='btn btn-outline-secondary'    
                                >
                                <Translate id="new_note"></Translate>
                            </NavLink></li>
                        </ul>
                        <LanguageToggle/>
                        </div>
                    </nav>

                </header>
                <Route path="/posts/:id/update" exact component={EditNote} />
                <Route path="/"  exact component={Notes} />
                <Route path="/new-post" component={NewNote} />
                <Route path="/posts/:id" exact component={FullNote} />


            </div>
        );
    }
}

export default withLocalize(Home);