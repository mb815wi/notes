import React from "react";
import { withLocalize } from "react-localize-redux";

const LanguageToggle = ({ languages, activeLanguage, setActiveLanguage }) => (
  <ul className="navbar-nav">
    {languages.map(lang => (
      <li className={lang.code === activeLanguage.code ? "nav-item btn btn-info" : "nav-item btn"} key={lang.code}>
        <a onClick={() => setActiveLanguage(lang.code)}>
          {lang.name}
        </a>
      </li>
    ))}
  </ul>
);

export default withLocalize(LanguageToggle);