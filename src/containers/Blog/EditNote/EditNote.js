import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import './EditNote.css';

class EditNote extends Component {

    state = {
        title: '',
        body: '',
        submitted: false,
        errMessage: ''
    }

    componentDidMount() {
        axios.get('/posts/' + this.props.match.params.id)
            .then(response => {
                console.log(response.data);
                this.setState({ title: response.data.title });
                this.setState({ body: response.data.body });
            });
    }

    postDataHandler = () => {
        const data = {
            title: this.state.title,
            body: this.state.body,
        };
        if(!data.title.length || !data.body.length){
            this.setState({errMessage:'Všetky polia musia byť vyplnené!'})
        }else{
            axios.put('/posts/' + this.props.match.params.id, data)
            .then(response => {
                console.log(response);
                this.setState( { submitted: true } );
                this.props.history.replace('/');
            });
        }


        
    }

    render() {
        let redirect = null;
        if (this.state.submitted) {
            redirect = <Redirect to="/" />;
        }
        let errMessage = '';
        if(this.state.errMessage){
            errMessage = <div className="alert alert-danger" role="alert">{this.state.errMessage}</div>
        }

        return (
            <div className="FullNote col-md-3 container">
                {errMessage ? errMessage : ''}
                {redirect}
                <h1>Edit a Post</h1>

                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input type="text" value={this.state.title} className="form-control" id="title" onChange={(event) => this.setState({ title: event.target.value })} />
                </div>
                <div className="form-group">
                    <label htmlFor="body">Post</label>
                    <textarea className="form-control" id="body" onChange={(event) => this.setState({ body: event.target.value })} rows="3" value={this.state.body}></textarea>
                </div>

                <button onClick={this.postDataHandler} className="btn  btn-primary">Edit Post</button>
            </div>
        );
    }
}

export default EditNote;