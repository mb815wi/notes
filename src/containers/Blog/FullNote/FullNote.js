import React, { Component } from 'react';
import axios from 'axios';

import './FullNote.css';

class FullNote extends Component {
    state = {
        loadedPost: null
    }

    componentDidMount () {
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    loadData () {
        if ( this.props.match.params.id ) {
            if ( !this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== +this.props.match.params.id) ) {
                axios.get( '/posts/' + this.props.match.params.id )
                    .then( response => {
                        this.setState( { loadedPost: response.data } );
                    } );
            }
        }
    }

    deletePostHandler = () => {
        axios.delete('/posts/' + this.props.match.params.id)
            .then(response => {
                this.props.history.replace('/');
            });
    }

    editPostHandler = () => {
        this.props.history.push( '/posts/'+ this.props.match.params.id  +'/update');
                
    }

    render () {
        let post = <p style={{ textAlign: 'center' }}>Please select a Post!</p>;
        if ( this.props.match.params.id ) {
            post = <p style={{ textAlign: 'center' }}>Loading...!</p>;
        }
        if ( this.state.loadedPost ) {
            post = (
                <div className="FullNote col-md-3 ">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                   <div className="row">
                        <div className="col-md-2 offset-md-4">
                            <button onClick={this.deletePostHandler} className="btn btn-danger">Delete</button>    
                        </div>
                        <div className="col-md-2">
                            <button onClick={this.editPostHandler} className="btn btn-info">Edit</button>    
                        </div>
                   </div>
                </div>

            );
        }
        return post;
    }
}

export default FullNote;