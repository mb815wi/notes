import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { Translate } from "react-localize-redux";

import './NewNote.css';

class NewNote extends Component {
    state = {
        title: '',
        content: '',
        submitted: false,
        errMessage: ''
    }

    postDataHandler = () => {
        const data = {
            title: this.state.title,
            body: this.state.content,
        };

        if(!data.title.length || !data.body.length){
            this.setState({errMessage:'Všetky polia musia byť vyplnené!'})
        }else{
            axios.post( '/posts', data )
                .then( response => {
                    this.setState( { submitted: true } );
                    this.props.history.replace('/');
            });
        }

    }

    render () {
        let redirect = null;
        if (this.state.submitted) {
            redirect = <Redirect to="/posts" />;
        }
        let errMessage = '';
        if(this.state.errMessage){
            errMessage = <div className="alert alert-danger" role="alert">{this.state.errMessage}</div>
        }
        

        return (
            <div className="NewNote container col-md-3">
                {errMessage ? errMessage : ''}
                {redirect}
                <h1><Translate id="new_note"></Translate></h1>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input type="text" value={this.state.title} className="form-control" id="title" onChange={( event ) => this.setState( { title: event.target.value } )} />
                </div>
                <div className="form-group">
                    <label htmlFor="body">Post</label>
                    <textarea className="form-control" id="body" onChange={( event ) => this.setState( { content: event.target.value } )} rows="3" value={this.state.content}></textarea>
                </div>
                <button onClick={this.postDataHandler} className="btn  btn-primary">Add Post</button>
            </div>
        );
    }
}

export default NewNote;