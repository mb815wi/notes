import React, { Component } from 'react';
import axios from '../../../axios';
import { Route } from 'react-router-dom';

import Note from '../../../components/Note/Note';
import './Notes.css';
import FullNote from '../FullNote/FullNote';

class Notes extends Component {
    state = {
        posts: []
    }

    componentDidMount() {
        console.log(this.props);
        axios.get('/posts')
            .then(response => {
                const posts = response.data;
                const updatedPosts = posts.map(post => {
                    return {
                        ...post
                    }
                });
                this.setState({ posts: updatedPosts });
            })
            .catch(error => {
                console.log(error);
            });
    }

    postSelectedHandler = (id) => {
        this.props.history.push('/posts/' + id);
    }

    render() {
        let posts = <p style={{ textAlign: 'center' }}>Something went wrong!</p>;
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return (
                    <Note
                        key={post.id}
                        title={post.title}
                        body={post.body.slice(0, 150)}
                        clicked={() => this.postSelectedHandler(post.id)} />
                );
            });
        }

        return (
            <div className="container">
            <div className="row">
            
                    {posts}
                    </div>
                <Route path={this.props.match.url + '/:id'} exact component={FullNote} />
            </div>
        );
    }
}

export default Notes;