import React from 'react';

import './Note.css';

const note = (props) => (
    <div className="col-md-3 mt-4">
        <div className="note" onClick={props.clicked}>
            <h1>{props.title}</h1>
            <div className="Info">
                {props.body}
            </div>
        </div>
    </div>
);

export default note;