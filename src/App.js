import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Home from './containers/Blog/Home';
import { LocalizeProvider } from "react-localize-redux";

class App extends Component {
  render() {
    return (
      <LocalizeProvider>
        <BrowserRouter>
          <div className="App">
            <Home />
          </div>
        </BrowserRouter>
      </LocalizeProvider>
    );
  }
}

export default App;
